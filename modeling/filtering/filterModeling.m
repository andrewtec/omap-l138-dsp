clear
clc
close all
format longG

%fir filters
lpf = fir1(40, 1670/(16000/2), 'low');
lpf'
fvtool(lpf)

hpf = fir1(40, 4650/(16000/2), 'high');
hpf'
fvtool(hpf)

bpf = fir1(40, [1670/(16000/2), 4650/(16000/2)], 'bandpass');
bpf'
fvtool(bpf)

%iir filters
[lpfB,lpfA] = cheby1(10,0.5,1670/8000,'low');
lpfB'
lpfA'
fvtool(lpfB,lpfA)

[hpfB,hpfA] = cheby1(10,0.5,4650/8000,'high');
hpfB'
hpfA'
fvtool(hpfB,hpfA)

[bpfB, bpfA] = cheby1(10,0.5,[1670/8000, 4650/8000], 'bandpass');
bpfB'
bpfA'
fvtool(bpfB,bpfA)