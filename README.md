# TI OMAP-L138 DSP Testing

The goal is to utilize Texas Instrument's [OMAP‐L138 processor](http://www.ti.com/tool/tmdslcdk138) to apply various DSP processes to test the OMAP-L138 processor.

![OMAP-L138](https://www.ti.com/diagrams/med_tmdslcdk138_tmdslcdk138_image.jpg "Development Kit")

## Testing Features

1. Sine Wave Generation
2. Square Wave Generation
3. Pseudo Random Sequencing
4. FIR Filtering
5. IIR Filtering
6. Noise Cancellation

## Baseline Response

Outputs of the signal when initially tested.

### Sine Wave Generation

<img src="previews/sineWave.png"  width="640" height="480">

### Square Wave Generation

<img src="previews/squareWave.png" width="640" height="480">

### Randomized Sequence

<img src="previews/randomRawOutput.png" width="640" height="480">

<img src ="previews/binaryResponse.png" width="640" height="480">

### FIR Filtering

#### Low Pass Filter

<img src="previews/fir_lpf_wav_input.png" width="640" height="480">

#### High Pass Filter

<img src ="previews/fir_hpf_wav_input.png" width="640" height="480">

#### Band Pass Filter

<img src ="previews/fir_bpf_wav_input.png" width="640" height="480">

### IIR Filtering

#### Low Pass Filter

<img src="previews/iir_lpf_sin_gen_input.png" width="640" height="480">

#### High Pass Filter

<img src="previews/iir_hpf_sin_gen_input.png" width="640" height="480">

#### Band Pass Filter

<img src="previews/iir_bpf_sin_gen_input.png" width="640" height="480">

### Noise Cancellation

Peak amplitude before noise cancellation : 54 mV

Peak amplitude after noise cancellation : 27 mV

**NOTE: This could be tested better...**

<img src="previews/oScopeNoiseCancel.png" width="640" height="480">

### 

## Modeling

### FIR Filters

#### Low Pass Filter

<img src="previews/lpfModel.png" width="940" height="480">

#### High Pass Filter

<img src="previews/hpfModel.png" width="940" height="480">

#### Band Pass Filter

<img src="previews/bpfModel.png" width="940" height="480">

### IIR Filters

#### Low Pass Filter
<img src="previews/lpfmodel_iir.png" width="940" height="480">

#### High Pass Filter
<img src="previews/hpfModel_iir.png" width="940" height="480">

#### Band Pass Filter
<img src="previews/bpfModel__iir.png" width="940" height="480">

## NOTES

### Tools and Setup

Use [Code Composer Studio](http://www.ti.com/tool/ccstudio), or [Eclipse](https://www.eclipse.org/), to successfully compile this project.

Add either of the following into CCS's symbols list and build variables:

`sineWave`

`squareWave`

`randomSequence`

`FIR`

`IIR`

`noiseCancel`

`noiseCancelNormalize`

### Signal Generation

Both `sineWave` and `squareWave` could have been implemented using a generated lookup table. This can still be the testing method if the tester uses the values found under the `/signalGeneration` directory.

## Pseudo Random Sequence

The sequence is NOT random since it is using the [LSFR](https://www.wikiwand.com/en/Linear-feedback_shift_register) technique. Since this device was tested using fixed point `Int16`, the period is determined to be `65,535` computational iterations.

![Polynomial](https://wikimedia.org/api/rest_v1/media/math/render/svg/0614dd0d8f82fe8220189d22bb9d97d7d8b087b9 "Feedback Polynomial")

### Filtering

A frequency sweep with an output trigger sync'd with the O-scope would have made a much better determining measurement of performance, but the tools were not available at the time.

**NOTE THE VARIOUS _TAP_ VALUES (number of coefficients)**

### Noise Cancellation and its Theory

Use [this](testFiles/nosieCancelTest_input.wav) file to test.

The noise cancellation test implements the gradient descent technique. This technique is used in conjunction with the FIR filter algorithm to adaptively modify the coefficients of the FIR filter. It converges to the optimal coefficients by steps of "mu" feeding back into the system (see below).

<img src="previews/gradientDescentBlockDiagram.png" width="970" height="480">

It is known that there is an signal attenuation factor of `sqrt(numberOfFIRCoefficients)`  when using this technique, so this factor is premptively normalized before it enters the FIR block.

To better tinker manipulate the signal, the `Int16` factor `32767` is divided to normalize the signal and then re-implemented at the output.

Various `MU` values are used to test how quickly the noise is cancelled from the initially given FIR coefficients. The tester should listen for noise "dipping" on the LEFT channel and a guitar playing becoming much more pronounced.