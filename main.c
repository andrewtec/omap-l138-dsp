///////////////////////////////////////////////////////////////////////
// Filename: main.c
//
// Synopsis: Main program file for demonstration code
//
///////////////////////////////////////////////////////////////////////

#include "DSP_Config.h"   
#include "LCDK_Support_DSP.h"

int main()
{

    // initialize DSP board
    DSP_Init();

    // initialize the LEDs
    InitLEDs();

    //Initialize digital outputs (switches)
    InitDigitalOutputs();


    while(1) {
    }
}